package com.codemibiles.firstavenger.basic_api.fragment_simple.data

import androidx.fragment.app.Fragment


class FragmentModel (
    val tabname:String,
    val fragment: Fragment
)