package com.codemibiles.firstavenger.basic_api.ui

import com.codemibiles.firstavenger.basic_api.data.BeerModel
import com.codemibiles.firstavenger.basic_api.service.BeerManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BeerPresenter(val view: BeerInterface) {

    fun getBeerApi() {
        BeerManager().createService().getRandomBeer().enqueue(object : Callback<List<BeerModel>> {
            override fun onFailure(call: Call<List<BeerModel>>, t: Throwable) {
                println("FAILED !")
            }

            override fun onResponse(call: Call<List<BeerModel>>, response: Response<List<BeerModel>>) {
                response.body()?.apply {
                    if (!this.isEmpty()) {
                        view.setBeer(this[0])
                    }
                }
            }
        })
    }
}
