package com.codemibiles.firstavenger.basic_api.fragment_simple.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.codemibiles.firstavenger.R

class ProfileFragment : Fragment() {

    companion object {
        fun intent(): ProfileFragment = ProfileFragment()
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_beer,container,false)
    }


}