package com.codemibiles.firstavenger.basic_api.fragment_simple

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import com.codemibiles.firstavenger.R
import com.codemibiles.firstavenger.basic_api.fragment_simple.data.FragmentModel
import com.codemibiles.firstavenger.basic_api.fragment_simple.ui.main.PlaceholderFragment
import com.codemibiles.firstavenger.basic_api.fragment_simple.ui.main.ProfileFragment
import com.codemibiles.firstavenger.basic_api.fragment_simple.ui.main.SectionsPagerAdapter

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setView()

    }

    fun setView() {
        var tabList = listOf<FragmentModel>(
            FragmentModel("Home",PlaceholderFragment.newInstance(123)),
            FragmentModel("Profile",ProfileFragment.intent())
        )


        val sectionsPagerAdapter = SectionsPagerAdapter(this,tabList,supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
    }


}