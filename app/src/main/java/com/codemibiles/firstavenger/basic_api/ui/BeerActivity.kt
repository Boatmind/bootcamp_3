package com.codemibiles.firstavenger.basic_api.ui

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.codemibiles.firstavenger.R
import com.codemibiles.firstavenger.basic_api.data.BeerModel
import com.codemibiles.firstavenger.basic_api.service.BeerManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_beer.*


class BeerActivity : AppCompatActivity(), BeerInterface {

    private val presenter = BeerPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer)
        presenter.getBeerApi()
        setView()
    }

    override fun setBeer(beerList: BeerModel) {
        println("boat")
        beerdesciption.text =  beerList.description
        Picasso.with(this).load(beerList.imageUrl).into(imageView);
    }

    private fun setView() {
        RandomBtn.setOnClickListener {
            presenter.getBeerApi()
        }
    }


}