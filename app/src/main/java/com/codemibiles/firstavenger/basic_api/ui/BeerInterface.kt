package com.codemibiles.firstavenger.basic_api.ui

import com.codemibiles.firstavenger.basic_api.data.BeerModel


interface BeerInterface {

    fun setBeer(beerList: BeerModel)
}